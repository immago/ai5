﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace AI5
{
    public partial class MainForm : Form
    {
        Dictionary<string, List<string>> objects = new Dictionary<string, List<string>>();
        //Противопоставления
        Dictionary<string, string> conflicts = new Dictionary<string, string>();
        //База правил ЕСЛИ-ТО
        Dictionary<string, string> alias = new Dictionary<string, string>();

        public MainForm()
        {
            InitializeComponent();

            ////База правил ЕСЛИ-ТО
            ////Признак - Признак или объект
            //alias.Add("престижн", @"Большая зарплата");
            //alias.Add("писать программы", @"Программист");

            ////Противопоставления
            //conflicts.Add(@"Низк[\s\S]* квалификац", @"Высок[\s\S]* квалификац");
            //conflicts.Add(@"Больш[\s\S]* \bзарплат", @"Низк[\s\S]* \bзарплат");

            //conflicts.Add(@"Техническ[\s\S]* специальност", @"Гуманитарн[\s\S]* специальност");
            //conflicts.Add(@"Медецин[\s\S]* специальност", @"Техническ[\s\S]* специальност");
            //conflicts.Add(@"Гуманитарн[\s\S]* специальност", @"Медецин[\s\S]* специальност");

            //// Обект - признак (regex)
            //objects.Add("Программист", new List<string>() { @"Больш[\s\S]* \bзарплат", @"Высок[\s\S]* квалификац", @"Техническ[\s\S]* специальност" });
            //objects.Add("Дворник", new List<string>() { @"Низк[\s\S]* \bзарплат", @"Низк[\s\S]* квалификац", @"Гуманитарн[\s\S]* специальност" });
            //objects.Add("Врач", new List<string>() { @"Высок[\s\S]* квалификац", @"Медецин[\s\S]* специальност" });

            //Искомый запрос
            //string text = "Хочу работу с большой зарплатой ИЛИ низкой квалификацей";
            //string text = "Хочу престижную работу";
            //string text = "Большая зарплата и низкая зарплата";

          
            updateObjectsList();
            updateConflictsList();
            updateRulesList();
            
        }

        void updateObjectsList()
        {
            listViewObjects.Items.Clear();
            foreach (var x in objects.Select((Entry, Index) => new { Entry, Index }))
            {
                string feach = "";
                foreach (string par in x.Entry.Value)
                    feach += par + " ";

                listViewObjects.Items.Add(x.Entry.Key).SubItems.Add(feach);
            }
        }

        void updateConflictsList()
        {
            listViewConflicts.Items.Clear();
            foreach (var x in conflicts.Select((Entry, Index) => new { Entry, Index }))
                listViewConflicts.Items.Add(x.Entry.Key).SubItems.Add(x.Entry.Value);
            
        }

        void updateRulesList()
        {
            listViewRules.Items.Clear();
            foreach (var x in alias.Select((Entry, Index) => new { Entry, Index }))
                listViewRules.Items.Add(x.Entry.Key).SubItems.Add(x.Entry.Value);

        }

        private void buttonObjectAdd_Click(object sender, EventArgs e)
        {
            if(objects.ContainsKey(textBoxObjectName.Text))
            {
                objects[textBoxObjectName.Text].Add(textBoxObjectAtribute.Text);
            }else
            {
                objects.Add(textBoxObjectName.Text, new List<string>() { textBoxObjectAtribute.Text });
            }

            updateObjectsList();
        }

        private void buttonObjectRemove_Click(object sender, EventArgs e)
        {
            if(listViewObjects.SelectedItems.Count != 0)
            {
                objects.Remove(listViewObjects.SelectedItems[0].Text);
                updateObjectsList();
            }
        }

        private void listViewObjects_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonConflictsAdd_Click(object sender, EventArgs e)
        {
            conflicts.Add(textBoxConflicts1.Text, textBoxConflicts2.Text);
            updateConflictsList();
        }

        private void buttonConflictsRemove_Click(object sender, EventArgs e)
        {
            if (listViewConflicts.SelectedItems.Count != 0)
            {
                conflicts.Remove(listViewConflicts.SelectedItems[0].Text);
                updateConflictsList();
                
            }
        }

        private void buttonRuleAdd_Click(object sender, EventArgs e)
        {
            alias.Add(textBoxRuleIF.Text, textBoxRuleTHEN.Text);
            updateRulesList();
        }

        private void buttonRuleRemove_Click(object sender, EventArgs e)
        {
            if (listViewRules.SelectedItems.Count != 0)
            {
                alias.Remove(listViewRules.SelectedItems[0].Text);
                updateRulesList();

            }
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            Interpritator i = new Interpritator();
            List<string> res = i.getResults(objects, conflicts, alias, textBoxSearch.Text);

            if (res.Count == 0)
            {
                MessageBox.Show("Ничего не найдено.");
            }
            else
            {
                string result = "Вам подойдет: \n";
                foreach (string re in res)
                    result += re + "\n";
                MessageBox.Show(result);
            }
        }

        //save & load
        

        private void buttonSave_Click(object sender, EventArgs e)
        {
           
            
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SaveObject save = new SaveObject();
                save.objects = objects;
                save.conflicts = conflicts;
                save.alias = alias;
               
                Serializer ser = new Serializer();
                ser.SerializeObject(saveFileDialog1.FileName, save);
            }
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.RestoreDirectory = true;

            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Serializer ser = new Serializer();
                SaveObject load = ser.DeSerializeObject(openFileDialog1.FileName);
                objects = load.objects;
                conflicts = load.conflicts;
                alias = load.alias;

                updateObjectsList();
                updateConflictsList();
                updateRulesList();

            }
        }
    }
}
