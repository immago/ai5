﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

public class Serializer
{
    public Serializer()
    {
    }

    public void SerializeObject(string filename, SaveObject objectToSerialize)
    {
        Stream stream = File.Open(filename, FileMode.Create);
        BinaryFormatter bFormatter = new BinaryFormatter();
        bFormatter.Serialize(stream, objectToSerialize);
        stream.Close();
    }

    public SaveObject DeSerializeObject(string filename)
    {
        SaveObject objectToSerialize;
        Stream stream = File.Open(filename, FileMode.Open);
        BinaryFormatter bFormatter = new BinaryFormatter();
        objectToSerialize = (SaveObject)bFormatter.Deserialize(stream);
        stream.Close();
        return objectToSerialize;
    }
}

[Serializable]
public class SaveObject : ISerializable
{
    public Dictionary<string, List<string>> objects;
    public Dictionary<string, string> conflicts;
    public Dictionary<string, string> alias;


   public SaveObject()
   {
   }

   public SaveObject(SerializationInfo info, StreamingContext ctxt)
   {
       this.objects = (Dictionary<string, List<string>>)info.GetValue("objects", typeof(Dictionary<string, List<string>>));
       this.conflicts = (Dictionary<string, string>)info.GetValue("conflicts", typeof(Dictionary<string, string>));
       this.alias = (Dictionary<string, string>)info.GetValue("alias", typeof(Dictionary<string, string>));

   }

   public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
   {
       info.AddValue("conflicts", this.conflicts);
       info.AddValue("objects", this.objects);
       info.AddValue("alias", this.alias);
   }
}